package com.example.jharshman.jdharshmanfinal;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import java.util.Calendar;
import java.util.Random;

/**
 * Created by jharshman on 12/1/15.
 */

public class GameView extends View {

    int tapCount = 0;
    int firstTap;
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        Log.d("DEBUG: ", "VIEW TOUCHED");

        if(!mCheatCode) {

            Calendar calendar = Calendar.getInstance();

            tapCount++;
            if(tapCount == 2) {
                int secondTap = calendar.get(Calendar.SECOND);
                if((secondTap - firstTap) < 2) {
                    //enable cheat
                    mCheatCode = true;
                    mListener.cheater(true);
                    Log.d("DEBUG", "CHEAT ENABLED");
                }
            } else if(tapCount > 2) {
                tapCount = 0;
            }

            // two taps within 2000 milliseconds will activate the cheat code
            // get current time of tap and subtract it from subsequent tap
            firstTap = calendar.get(Calendar.SECOND);


        }
        return super.onTouchEvent(event);
    }

    /**
     * Implement simple listener to get up to date score info in main activity
     * */
    public interface CakeListener {
        void onStateChanged(int cakes);
        void onDeath(boolean state);
        void cheater(boolean state);
    }
    private CakeListener mListener = null;
    public void registerListener(CakeListener listener) {
        mListener = listener;
    }

    /**
     * Character class.  Encapsulates game character data.
     * All moving objects in game are Characters
     * */
    private class Character {
        private int row, col;
        private String currentDirection;
        Character(int row, int col) {
            this.row = row;
            this.col = col;
        }
        String getCurrentDirection() { return currentDirection; }
        void setCurrentDirection(String currentDirection ) { this.currentDirection = currentDirection; }
        int getRow() { return row; }
        int getCol() { return col; }
        void setRow(int row) { this.row = row; }
        void setCol(int col) { this.col = col; }
    }

    /*
    * static variables
    * */
    final int UP            = 0;
    final int DOWN          = 1;
    final int LEFT          = 2;
    final int RIGHT         = 3;
    final int HEIGHT        = 14;
    final int WIDTH         = 14;
    final int RED           = Color.rgb(255, 0, 0);
    final int BLACK         = Color.rgb(0, 0, 0);
    final int YELLOW        = Color.rgb(205, 255, 0);
    final int WHITE         = Color.rgb(255, 255, 255);
    final int BLUE          = Color.rgb(22, 22, 248);

    /*
    * Class private variables
    * */
    private int mNumCakes;
    private Paint mPaint;
    private float mScaledWidth;
    private float mScaledHeight;
    private Character mGhostOne;
    private Character mGhostTwo;
    private Character mGhostThree;
    private Character mPacman;
    private boolean mYouLose;
    private boolean mCheatCode = false;
    /**
     * Note:
     * Storing this representation fo the game grid has the benefits of
     * 1. Simple initial placement of elements onto the board
     * 2. Assigning unique identifiers to the different elements
     * 3. Provides it's own simplistic collision detection
     * */
    private int[][] gameBoard = {

        {0,0,0,0,0,0,0,0,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,3,0,0,3,0,0,0},
        {0,3,3,3,3,3,3,3,0,0,3,0,0,0},
        {0,0,0,0,0,0,0,0,0,0,3,0,0,0},
        {0,0,0,0,0,3,0,0,0,0,3,0,0,0},
        {0,0,0,0,0,3,0,0,0,0,3,0,0,0},
        {0,0,2,0,0,3,0,1,0,0,0,0,3,0},
        {0,0,0,0,0,3,0,0,0,0,0,0,3,0},
        {0,0,0,0,0,3,0,0,0,0,0,0,3,0},
        {0,0,0,0,0,3,0,0,0,2,0,0,3,0},
        {0,3,3,3,3,3,3,0,0,0,0,0,3,0},
        {0,0,0,0,0,0,0,0,0,0,0,0,3,0},
        {0,0,2,0,0,0,0,0,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,0,0,3,3,3,3,3},

        /*
        0 = cake
        1 = pacman
        2 = ghost
        3 = wall
         */

    };



    /**
     * Animation Timer
     * Handles the movement of objects on the board by updating the gameBoard matrix
     * */
    Handler handler = new Handler();
    Runnable runnable = new Runnable() {
        @Override
        public void run() {

            // handle movement of ghosts
            // move ghosts around the grid by updating the gameBoard matrix

            if(!mYouLose && mNumCakes != 0) {
                //first ghost
                move(mGhostOne);

                //second ghost
                move(mGhostTwo);

                //third ghost
                move(mGhostThree);

                //move pacman
                movePacman(mPacman);

                // invalidate to cause redraw
                postInvalidate();
                handler.postDelayed(this, 500);
            }

        }
    };

    /**
     * Move function for ghost characters
     * Provides method of moving ghost characters
     * on the game board.
     * <p></p>
     * Provides wall collision detection and out of bounds detection.
     * Also calls a method checking for collision with the main character.
     *
     * @Param character     game character object
     * */
    private void move(Character character) {

        // we can then use the value to keep the ghost moving in that direction until it hits a wall in which case it would select
        // a new direction at random, start moving in that direction, and set the new current direction

        /*
        final int UP            = 0;
        final int DOWN          = 1;
        final int LEFT          = 2;
        final int RIGHT         = 3;
        */

        String currentDirection = character.getCurrentDirection();

        Random rand = new Random();
        int movement = rand.nextInt(4);

        /* Switch up columns and rows because strange coordinate system */
        int col = character.getRow();
        int row = character.getCol();
        int element;
        // set current direction to null at start
        // if current direction is null, select a direction at random
        // if a direction doesn't pan out, set the current direction to null
        try {
            if (currentDirection.equals("up")) {

                isPacman(UP, row, col);

                if (isAvailable(UP, row, col)) {
                    // can move up
                    character.setCurrentDirection("up");
                    element = gameBoard[row-1][col];
                    gameBoard[row - 1][col] = gameBoard[row][col];
                    gameBoard[row][col] = element;
                    character.setCol(row - 1);
                } else {
                    character.setCurrentDirection("right");
                }
            } else if (currentDirection.equals("down")) {

                isPacman(DOWN, row, col);

                if (isAvailable(DOWN, row, col)) {
                    // can move down
                    character.setCurrentDirection("down");
                    element = gameBoard[row+1][col];
                    gameBoard[row + 1][col] = gameBoard[row][col];
                    gameBoard[row][col] = element;
                    character.setCol(row + 1);
                } else {
                    character.setCurrentDirection("left");
                }
            } else if (currentDirection.equals("left")) {

                isPacman(LEFT, row, col);

                if (isAvailable(LEFT, row, col)) {
                    // can move left
                    character.setCurrentDirection("left");
                    element = gameBoard[row][col-1];
                    gameBoard[row][col - 1] = gameBoard[row][col];
                    gameBoard[row][col] = element;
                    character.setRow(col - 1);
                } else {
                    character.setCurrentDirection("up");
                }
            } else if (currentDirection.equals("right")) {

                isPacman(RIGHT, row, col);

                if (isAvailable(RIGHT, row, col)) {
                    // can move right
                    character.setCurrentDirection("right");
                    element = gameBoard[row][col+1];
                    gameBoard[row][col + 1] = gameBoard[row][col];
                    gameBoard[row][col] = element;
                    character.setRow(col + 1);
                } else {
                    character.setCurrentDirection("down");
                }
            } else {
                if (movement == UP)
                    character.setCurrentDirection("up");
                else if (movement == DOWN)
                    character.setCurrentDirection("down");
                else if (movement == LEFT)
                    character.setCurrentDirection("left");
                else
                    character.setCurrentDirection("right");
            }
        } catch(IndexOutOfBoundsException ioobe) {
            character.setCurrentDirection("not");
        }

    }

    /**
     * Tweaked version of move(Character character) function which is used for ghosts.
     * Function is adapted to work for Pacman.  Similar logic, which means we could probably make one
     * generic move() function that works for both ghosts and pacman.  Current reason why they are
     * separate functions is due to the ghost AI logic in the move(Character character) function.
     *
     * @Param character   Game Character Object
     * */
    private void movePacman(Character character) {

        int col = character.getRow();
        int row = character.getCol();
        int element;
        String direction = character.getCurrentDirection();

        try {
            if(direction.equals("up")) {
                if(isAvailable(UP, row, col)) {
                    // move upwards
                    character.setCurrentDirection("up");
                    element = gameBoard[row-1][col];

                    if(element == 0) {
                        mNumCakes--;
                        if(mListener != null)
                            mListener.onStateChanged(mNumCakes);
                    }

                    gameBoard[row - 1][col] = gameBoard[row][col];
                    gameBoard[row][col] = 9;
                    character.setCol(row - 1);
                }
            } else if(direction.equals("down")) {
                if(isAvailable(DOWN, row, col)) {
                    // move downwards
                    character.setCurrentDirection("down");
                    element = gameBoard[row+1][col];

                    if(element == 0) {
                        mNumCakes--;
                        if(mListener != null)
                            mListener.onStateChanged(mNumCakes);
                    }

                    gameBoard[row + 1][col] = gameBoard[row][col];
                    gameBoard[row][col] = 9;
                    character.setCol(row + 1);
                }
            } else if(direction.equals("left")) {
                if(isAvailable(LEFT, row, col)) {
                    // move left
                    character.setCurrentDirection("left");
                    element = gameBoard[row][col-1];

                    if(element == 0) {
                        mNumCakes--;
                        if(mListener != null)
                            mListener.onStateChanged(mNumCakes);
                    }

                    gameBoard[row][col - 1] = gameBoard[row][col];
                    gameBoard[row][col] = 9;
                    character.setRow(col - 1);
                }
            } else if(direction.equals("right")) {
                if(isAvailable(RIGHT, row, col)) {
                    // move right
                    character.setCurrentDirection("right");
                    element = gameBoard[row][col+1];

                    if(element == 0) {
                        mNumCakes--;
                        if(mListener != null)
                            mListener.onStateChanged(mNumCakes);
                    }

                    gameBoard[row][col + 1] = gameBoard[row][col];
                    gameBoard[row][col] = 9;
                    character.setRow(col + 1);
                }
            }
            Log.d("DEBUG: ", "number of cakes "+getmNumCakes());
        } catch (IndexOutOfBoundsException ioobe) {
            // this should just cease the movement until the direction is updated
            character.setCurrentDirection("not");
        }

    }

    /**
     * sets the direction of pacman
     *
     * @Param direction   direction in which the character should be moving
     * */
    public void setDirection(String direction) {
        mPacman.setCurrentDirection(direction);
    }

    /**
     * Check if the next spot a Character is attempting to move to is valid.
     *
     * @param movement  current movement of character
     * @param row       current row of character
     * @param col       current column of character
     * @return          true/false depending on valid/invalid move
     */
    private boolean isAvailable(int movement, int row, int col) throws IndexOutOfBoundsException {
        switch(movement) {
            case UP:
                return (gameBoard[row-1][col] != 3 && gameBoard[row-1][col] != 2 && gameBoard[row-1][col] != 1);
            case DOWN:
                return (gameBoard[row+1][col] != 3 && gameBoard[row+1][col] != 2 && gameBoard[row+1][col] != 1);
            case LEFT:
                return (gameBoard[row][col-1] != 3 && gameBoard[row][col-1] != 2 && gameBoard[row][col-1] != 1);
            case RIGHT:
                return (gameBoard[row][col+1] != 3 && gameBoard[row][col+1] != 2 && gameBoard[row][col+1] != 1);

        }

        return false;
    }

    /**
     * Function used to test ghost collision with pacman
     *
     * @Param movement  current movement of character
     * @Param row       current row location of character
     * @Param col       current column location of character
     * */
    private void isPacman(int movement, int row, int col) {

        if(!mCheatCode) {

            switch(movement) {
                case UP:
                    if(gameBoard[row-1][col] == 1) {
                        mListener.onDeath(true);
                        mYouLose = true;
                    }
                    break;
                case DOWN:
                    if(gameBoard[row+1][col] == 1) {
                        mListener.onDeath(true);
                        mYouLose = true;
                    }
                    break;
                case LEFT:
                    if(gameBoard[row][col-1] == 1) {
                        mListener.onDeath(true);
                        mYouLose = true;
                    }
                    break;
                case RIGHT:
                    if(gameBoard[row][col+1] == 1) {
                        mListener.onDeath(true);
                        mYouLose = true;
                    }
                    break;
            }

        }

    }

    /**
     * Sub constructor for View.  Calls GameView(Context, AttributeSet)
     *
     * @Param context     the current context
     * */
    public GameView(Context context) {
        this(context, null);
    }

    /**
     * Sub constructor for the View.  Calls GameView(Context, AttributeSet, defStyle)
     *
     * @Param context         the current context for the view
     * @Param attributeSet    the passed in set of attributes for the view
     * */
    public GameView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    /**
     * Master constructor for the view
     *
     * @Param context         the current context for the view
     * @Param attributeSet    the passed in set of attributes for the view
     * @Param defStyle        the default style to apply to the view
     * */
    public GameView(Context context, AttributeSet attributeSet, int defStyle) {
        super(context, attributeSet, defStyle);

        // View initialization (all view init goes here)
        mNumCakes = 0;
        // get number of cakes
        for(int i[] : gameBoard)
            for(int j : i)
                if(j == 0)
                    mNumCakes++;

        // get new Paint object
        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

        // set the view background
        setBackgroundResource(R.drawable.background);

        // initialize the ghosts
        mGhostOne = new Character(2,6);
        mGhostOne.setCurrentDirection("right");
        mGhostTwo = new Character(9,9);
        mGhostTwo.setCurrentDirection("left");
        mGhostThree = new Character(2,12);
        mGhostThree.setCurrentDirection("right");

        mPacman = new Character(7,6);
        mPacman.setCurrentDirection("not");

        // start the handler
        handler.postDelayed(runnable, 1000); // pause before animation begins

    }

    /**
     * Simple get method for number of cakes
     *
     * @Return mNumCakes    returns number of cakes present on board
     * */
    public int getmNumCakes() {
        return mNumCakes;
    }

    /**
     * Calculates the measurements and scaling of the view
     *
     * @Param widthMeasureSpec          measurespec for width
     * @Param widthHeightMeasureSpec    measurespec for height
     * */
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        //super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        int width = (MeasureSpec.getSize(heightMeasureSpec) * 1);
        int height =(MeasureSpec.getSize(widthMeasureSpec) / 1);

        if(height > MeasureSpec.getSize(heightMeasureSpec)) height = MeasureSpec.getSize(heightMeasureSpec);
        else                                                width = MeasureSpec.getSize(widthMeasureSpec);

        setMeasuredDimension(width, height);
    }

    /**
     * Draw according to current values stored int he game board array
     *
     * @Param canvas    canvas for view
     * */
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        //scale the canvas
        float scaleWidth = mScaledWidth/(float)(WIDTH);
        float scaleHeight = mScaledHeight/(float)(HEIGHT);

        canvas.scale(scaleWidth, scaleHeight);

        // draw the game state according to the gameBoard matrix
        for(int i = 0; i < gameBoard[0].length; i++) {
            for(int j = 0; j < gameBoard.length; j++) {
                if(gameBoard[i][j] == 2) {
                    //place ghost at location
                    drawCharacter(canvas, i, j, (float).50, RED);
                } else if(gameBoard[i][j] == 1) {
                    //place pacman at location
                    drawCharacter(canvas, i, j, (float).50, YELLOW);
                } else if(gameBoard[i][j] == 0) {
                    // place cake
                    drawCharacter(canvas, i, j, (float).20, WHITE);
                } else if(gameBoard[i][j] == 3) {
                    drawBrick(canvas, j, i, BLACK);
                } else {
                    drawBrick(canvas, j, i, BLUE);
                }

            }
        }

    }

    /**
     * Draws a simple character on the canvas.  A character is represented by a circle
     * of a specified radius and color.
     *
     * @Param canvas    canvas for view
     * @Param xDim      X coordinate for placement
     * @Param yDim      Y coordinate for placement
     * @Param radius    Radius of circle
     * @Param color     Color of fill
     * */
    private void drawCharacter(Canvas canvas, int xDim, int yDim, float radius, int color) {
        // set the color of the character
        mPaint.setColor(color);
        // draw the dot on the map centered at the coordinates given
        // flip the xDim and yDim to preserve the orientation of game board
        canvas.drawCircle(yDim + (float) .5, xDim + (float) .5, radius, mPaint);
    }

    /**
     * Draws a simple wall on the canvas.  A wall is represented by a square
     * of specified dimensions and color
     *
     * @Param canvas    canvas for view
     * @Param xDim      X coordinate for placement
     * @Param yDim      Y coordinate for placement
     * @Param color     Color of fill
     * */
    private void drawBrick(Canvas canvas, int xDim, int yDim, int color) {
        mPaint.setColor(color);
        Rect rect = new Rect();
        rect.set(xDim, yDim, xDim + 1, yDim + 1);
        canvas.drawRect(rect, mPaint);
    }

    /**
     * Calculate scaled width and height for view
     *
     * @Param w     current width
     * @Param h     current height
     * @Param oldw  former width
     * @Param oldh  former height
     * */
    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mScaledWidth = w;
        mScaledHeight = h;
    }
}
