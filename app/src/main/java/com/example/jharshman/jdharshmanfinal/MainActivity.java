package com.example.jharshman.jdharshmanfinal;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, GameView.CakeListener {

    /**
     * Implementation of onStateChanged(int cakes) from GameView.CakeListener
     * Once called, it plays the pacman classic cake eating sound effect.
     * If there are no cakes left, this indicates a win which will display
     * a toast indicating this accompanied by a sound effect.
     *
     * @Param cakes   number of cakes present on game board
     * */
    @Override
    public void onStateChanged(int cakes) {

        // cake has been eaten
        // play cake eating music
        if(mMediaPlayer!=null)
            mMediaPlayer.release();
        mMediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.eatcake);
        mMediaPlayer.start();

        mCakesLeft = getString(R.string.scoreboard, cakes);
        mScoreBoard.setText(mCakesLeft);
        if(cakes == 0) {
            if(mMediaPlayer!=null)
                mMediaPlayer.release();
            mMediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.won);
            mMediaPlayer.start();

            Toast toast = Toast.makeText(this, "You Win", Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    /**
     * Implementation of onDeath(boolean state) from GameView.CakeListener
     * Once called, it plays game end sound effect and reports the loss to screen
     * in the form of a toast
     *
     * @Param state   boolean value indicating the live/dead state of the main character
     * */
    @Override
    public void onDeath(boolean state) {
        // play death music
        if(mMediaPlayer!=null)
            mMediaPlayer.release();
        mMediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.death);
        mMediaPlayer.start();

        Toast toast = Toast.makeText(this, "Game Over", Toast.LENGTH_SHORT);
        toast.show();
    }

    /**
     * Implementation of cheater(boolean state) from GameView.CakeListener
     * Displays a simple notification toast to screen if a cheat code has
     * been activated
     *
     * @Param state   boolean value indicating the enabled/disabled state of the cheat code
     * */
    @Override
    public void cheater(boolean state) {
        if(state) {
            Toast toast = Toast.makeText(this, "Cheat C0d3z Enabled", Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    private String mCakesLeft;

    private GameView mGameView;
    private ImageView mCtrlUp;
    private ImageView mCtrlDown;
    private ImageView mCtrlLeft;
    private ImageView mCtrlRight;
    private TextView mScoreBoard;

    private AudioManager mAudioManager;
    private MediaPlayer mMediaPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mAudioManager = (AudioManager)getSystemService(AUDIO_SERVICE);
        mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC)/2, 0);

        mGameView = (GameView)findViewById(R.id.gameview);
        mGameView.registerListener(this);

        mScoreBoard = (TextView)findViewById(R.id.scoreboard);
        mCakesLeft = getString(R.string.scoreboard, mGameView.getmNumCakes());
        mScoreBoard.setText(mCakesLeft);

        mCtrlUp = (ImageView)findViewById(R.id.ctrlup);
        mCtrlDown = (ImageView)findViewById(R.id.ctrldown);
        mCtrlLeft = (ImageView)findViewById(R.id.ctrlleft);
        mCtrlRight = (ImageView)findViewById(R.id.ctrlright);

        mCtrlUp.setOnClickListener(this);
        mCtrlDown.setOnClickListener(this);
        mCtrlLeft.setOnClickListener(this);
        mCtrlRight.setOnClickListener(this);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_about) {
            aboutMe();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()) {
            case R.id.ctrlup:
                //move upwards
                mGameView.setDirection("up");
                Log.d("DEBUG: ", "Moving up");
                break;
            case R.id.ctrldown:
                //move downwards
                mGameView.setDirection("down");
                Log.d("DEBUG: ", "Moving down");
                break;
            case R.id.ctrlleft:
                //move left
                mGameView.setDirection("left");
                Log.d("DEBUG: ", "Moving left");
                break;
            case R.id.ctrlright:
                //move right
                mGameView.setDirection("right");
                Log.d("DEBUG: ", "Moving right");
                break;
        }
    }

    private void aboutMe() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle(R.string.attr_dialog_title)
                .setItems(R.array.about_me, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int i) {
                        dialog.dismiss();
                    }
                });
        builder.create();
        builder.show();
    }
}
